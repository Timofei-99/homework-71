import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {deleteOrder, getOrders} from "../../store/actions/menuAction";

const Orders = () => {
    const dispatch = useDispatch();
    const orders = useSelector((state) => state.menu.orders);
    const total = useSelector((state) => state.menu.price);

    useEffect(() => {
        dispatch(getOrders());
    }, [dispatch]);
    console.log(orders)

    const remove = (id) => {
        dispatch(deleteOrder(id))
    }

    return (
        <div>
            {orders.map((item) => {
                let totalPtice = 150;
                return (
                    <div style={{border: "1px solid black", marginBottom: "15px", padding: '20px'}}>
                        {Object.keys(item).map((product) => {
                            totalPtice += item[product].amount * item[product].price;
                            return (
                                <div key={item[product].id} style={{display: "flex", justifyContent: "space-around"}}>
                                    <p>Name: {item[product].title} </p>
                                    <p>Count: {item[product].amount} </p>
                                    <p>Price: {item[product].price} </p>
                                    <div>
                                        <p>Total: {item[product].amount * item[product].price} </p>
                                        <p onClick={() => remove(item[product].id)}>Complete</p>
                                    </div>
                                </div>
                            );
                        })}
                        <div style={{marginLeft: '78px', textAlign: 'center'}}>total:{totalPtice}</div>
                    </div>
                );
            })}
            ;
        </div>
    );
};

export default Orders;
