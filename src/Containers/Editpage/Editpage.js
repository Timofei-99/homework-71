import React, {useEffect, useState} from 'react';
import './Editpage.css';
import {useDispatch, useSelector} from "react-redux";
import {changeDish, putDish} from "../../store/actions/menuAction";

const Editpage = ({match, history}) => {
    const dispatch = useDispatch();
    const state = useSelector(state => state.menu.getInput);
    const id = match.params.id;
    const [input, setInput] = useState({
        title: '',
        price: '',
        image: '',
    });


    useEffect(() => {
        dispatch(changeDish(id));
        state.title && setInput(prevState => ({
            ...prevState,
            title: state.title,
            price: state.price,
            image: state.image
        }));
    }, [dispatch, state.title, state.price, state.image]);

    const handleInput = (e) => {
        const {name, value} = e.target;
        setInput(prev => ({...prev, [name]: value}));
    };

    const put = async (e)  => {
        e.preventDefault();
        await dispatch(putDish(id, input));
        history.push('/');
    };

    return (
        <>
            <form onSubmit={put}>
                <input
                    className="Input"
                    type="text"
                    name="title"
                    placeholder="Title"
                    onChange={handleInput}
                    value={input.title}
                />
                <input
                    className="Input"
                    type="text"
                    name="price"
                    placeholder="Price"
                    onChange={handleInput}
                    value={input.price}
                />
                <input
                    className="Input"
                    type="text"
                    name="image"
                    placeholder="Image"
                    onChange={handleInput}
                    value={input.image}
                />
                <button className='sendBtn'>Save changes</button>
            </form>
        </>
    );
};

export default Editpage;