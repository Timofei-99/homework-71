import React, {useEffect} from 'react';
import './Mainpage.css';
import {useDispatch, useSelector} from "react-redux";
import {fetchMenu, openModal, removeDish} from "../../store/actions/menuAction";
import Pizza from "../../Components/Pizza/Pizza";
import Modal from "../../Components/UI/Modal/Modal";

const Mainpage = ({match}) => {
    const id = match.params.id;
    console.log(id)
    const dispatch = useDispatch();
    const menu = useSelector(state => state.menu.menu);
    const modal = useSelector(state => state.menu.modal);

    useEffect(() => {
        dispatch(fetchMenu())
    }, [dispatch]);

    const open = () => {
        dispatch(openModal())
    };

    const remove = async (id) => {
       await dispatch(removeDish(id));
    };



    return (
        <div className='pizza'>
            <div className="title">
                <h1>Dishes</h1>
                <button onClick={open}>Add new Dish</button>
            </div>
            {menu.map(dish => (
                <Pizza
                    key={dish.title}
                    title={dish.title}
                    price={dish.price}
                    img={dish.image}
                    id={dish.id}
                    remove={() => remove(dish.id)}
                />
            ))}
            <Modal
                show={modal}
            />
        </div>
    );
};

export default Mainpage;