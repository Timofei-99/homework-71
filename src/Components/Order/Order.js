import React from "react";
import {useDispatch} from "react-redux";
import "./Order.css";

const Order = (props) => {
    const dispatch = useDispatch();



    return (
        <div className="Order">
            <div>
                {props.order}
                <p>Delivery :150KGS</p>
            </div>

            <div>
                <p>Total:{props.totalPrice}</p>
                <button>Complete Order</button>
            </div>
        </div>
    );
};

export default Order;
