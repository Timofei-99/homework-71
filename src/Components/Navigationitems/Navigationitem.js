import React from 'react';
import './Navigationitem.css';
import {NavLink} from "react-router-dom";

const Navigationitem = ({to, exact, children}) => {
    return (
        <li className="NavigationItem">
            <NavLink to={to} exact={exact}>{children}</NavLink>
        </li>
    );
};

export default Navigationitem;