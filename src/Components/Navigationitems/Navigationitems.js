import React from 'react';
import './Navigationitems.css';
import NavigationItem from "./Navigationitem";


const Navigationitems = () => {
    return (
        <ul className="NavigationItems">
            <NavigationItem to="/" exact>Dishes</NavigationItem>
            <NavigationItem to="/orders">Orders</NavigationItem>
        </ul>
    );
};

export default Navigationitems;