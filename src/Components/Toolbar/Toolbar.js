import React from 'react';
import './Toolbar.css';
import NavigationItems from "../Navigationitems/Navigationitems";
import Logo from "../UI/Logo/Logo";

const Toolbar = () => {
    return (
        <header className="Toolbar">
            <div className="Toolbar-logo">
                <Logo/>
            </div>
            <nav>
                <NavigationItems/>
            </nav>
        </header>
    );
};

export default Toolbar;