import React from 'react';
import './Header.css';
import {NavLink} from "react-router-dom";

const Header = () => {
    return (
        <header className='header'>
            <h2>Turtle Pizza Admin</h2>
            <ul>
                <li>
                    <NavLink to={'/'} exact>Dishes</NavLink>
                </li>
                <li>
                    <NavLink to={'/orders'}>Orders</NavLink>
                </li>
            </ul>
        </header>
    );
};

export default Header;