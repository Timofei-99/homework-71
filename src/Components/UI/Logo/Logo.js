import React from 'react';
import './Logo.css';

const Logo = () => {
    return (
        <div className="Logo">
            <h2>Turtle Pizza Admin</h2>
        </div>
    );
};

export default Logo;