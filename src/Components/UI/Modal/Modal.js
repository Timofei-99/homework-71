import React, {useState} from 'react';
import './Modal.css';
import Backdrop from "../Backdrop/Backdrop";
import {useDispatch} from "react-redux";
import {closeModal, postMenu} from "../../../store/actions/menuAction";

const Modal = (props) => {
    const dispatch = useDispatch();
    const [addDish, setAddDish] = useState({
        title: '',
        price: 0,
        image: ''
    });

    const change = (e) => {
        const {name, value} = e.target;
        setAddDish((prev) => ({...prev, [name]: value}));
    };

    const close = () => {
        dispatch(closeModal());
    };

    const submit = e => {
        e.preventDefault();
        dispatch(postMenu(addDish))
    };

    return (
        <>
            <Backdrop
                show={props.show}
                onClick={props.close}
            />
            <div
                className="Modal"
                style={{
                    transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                    opacity: props.show ? '1' : '0',
                }}
            >
                <div className='inputs'>
                    <h2>Оформить заказ</h2>
                    <form onSubmit={submit}>
                        <div>
                            <input className='inpt' name={"title"} value={addDish.title} placeholder='Title' type="text" onChange={change}/>
                        </div>
                        <div>
                            <input className='inpt' name={"price"} value={addDish.price} placeholder='Price' type="text" onChange={change}/>
                        </div>
                        <div>
                            <input className='inpt' name={"image"} value={addDish.image} placeholder='Image' type="text" onChange={change}/>
                        </div>
                        <button className='submitBtn' type={"submit"}>Отправить заказ</button>
                        <button onClick={close}  className='closeBtn' type={"button"}>Закрыть</button>
                    </form>
                </div>

            </div>
        </>
    );
};

export default Modal;