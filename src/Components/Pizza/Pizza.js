import React from 'react';
import './Pizza.css';
import {NavLink} from "react-router-dom";

const Pizza = ({img, title, price, id, remove}) => {
    return (
        <div className='card'>
            <img style={{width: '300px', height: '200px'}} src={img} alt=""/>
                <h3>{title}</h3>
                <span>{price} KGS</span>
            <NavLink to={`/edit/${id}`} className='addBtn'>Edit</NavLink>
            <button onClick={() => remove(id)} className='addBtn'>Delete</button>
        </div>
    );
};

export default Pizza;