import {
    CHANGE_DISH_FAILURE,
    CHANGE_DISH_REQUEST,
    CHANGE_DISH_SUCCESS,
    CLOSE_MODAL,
    DELETE_DISH_FAILURE,
    DELETE_DISH_REQUEST,
    DELETE_DISH_SUCCESS, DELETE_ORDER_FAILURE,
    DELETE_ORDER_REQUEST,
    DELETE_ORDER_SUCCESS,
    FETCH_MENU_FAILURE,
    FETCH_MENU_REQUEST,
    FETCH_MENU_SUCCESS,
    GET_ORDERS_FAILURE,
    GET_ORDERS_REQUEST,
    GET_ORDERS_SUCCESS,
    OPEN_MODAL,
    POST_MENU_FAILURE,
    POST_MENU_REQUEST,
    POST_MENU_SUCCESS,
    PUT_DISH_FAILURE,
    PUT_DISH_REQUEST,
    PUT_DISH_SUCCESS
} from "../actions/menuAction";

const initialState = {
    menu: [],
    modal: false,
    getInput: {},
    orders: [],
    price: 0,
};

const menuReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MENU_REQUEST:
            return {...state}
        case FETCH_MENU_SUCCESS:
            return {...state, menu: action.payload}
        case FETCH_MENU_FAILURE:
            return {...state}
        case OPEN_MODAL:
            return {...state, modal: true}
        case CLOSE_MODAL:
            return {...state, modal: false}
        case POST_MENU_REQUEST:
            return {...state}
        case POST_MENU_SUCCESS:
            return {...state, modal: false}
        case POST_MENU_FAILURE:
            return {...state}
        case CHANGE_DISH_REQUEST:
            return {...state}
        case CHANGE_DISH_SUCCESS:
            return {...state, getInput: action.payload}
        case CHANGE_DISH_FAILURE:
            return {...state}
        case PUT_DISH_REQUEST:
            return {...state}
        case PUT_DISH_SUCCESS:
            return {...state}
        case PUT_DISH_FAILURE:
            return {...state}
        case DELETE_DISH_REQUEST:
            return {...state}
        case DELETE_DISH_SUCCESS:
            return {...state, menu: action.payload}
        case DELETE_DISH_FAILURE:
            return {...state}
        case GET_ORDERS_REQUEST:
            return {...state}
        case GET_ORDERS_SUCCESS:
            return {...state, orders: action.payload}
        case GET_ORDERS_FAILURE:
            return {...state}
        case DELETE_ORDER_REQUEST:
            return {...state}
        case DELETE_ORDER_SUCCESS:
            return {...state, orders: action.payload}
        case DELETE_ORDER_FAILURE:
            return {...state}
        default:
            return state
    }
};

export default menuReducer;