import axiosAPI from "../../axiosAPI";
import BASE_PRICE from "../../constans";

export const FETCH_MENU_REQUEST = 'FETCH_MENU_REQUEST';
export const FETCH_MENU_SUCCESS = 'FETCH_MENU_SUCCESS';
export const FETCH_MENU_FAILURE = 'FETCH_MENU_FAILURE';

export const POST_MENU_REQUEST = 'POST_MENU_REQUEST';
export const POST_MENU_SUCCESS = 'POST_MENU_SUCCESS';
export const POST_MENU_FAILURE = 'POST_MENU_FAILURE';

export const CHANGE_DISH_REQUEST = 'CHANGE_DISH_REQUEST';
export const CHANGE_DISH_SUCCESS = 'CHANGE_DISH_SUCCESS';
export const CHANGE_DISH_FAILURE = 'CHANGE_DISH_FAILURE';

export const PUT_DISH_REQUEST = 'PUT_DISH_REQUEST';
export const PUT_DISH_SUCCESS = 'PUT_DISH_SUCCESS';
export const PUT_DISH_FAILURE = 'PUT_DISH_FAILURE';

export const DELETE_DISH_REQUEST = 'DELETE_DISH_REQUEST';
export const DELETE_DISH_SUCCESS = 'DELETE_DISH_SUCCESS';
export const DELETE_DISH_FAILURE = 'DELETE_DISH_FAILURE';

export const GET_ORDERS_REQUEST = 'GET_ORDERS_REQUEST';
export const GET_ORDERS_SUCCESS = 'GET_ORDERS_SUCCESS';
export const GET_ORDERS_FAILURE = 'GET_ORDERS_FAILURE';

export const DELETE_ORDER_REQUEST = 'DELETE_ORDER_REQUEST';
export const DELETE_ORDER_SUCCESS = 'DELETE_ORDER_SUCCESS';
export const DELETE_ORDER_FAILURE = 'DELETE_ORDER_FAILURE';

export const deleteOrderRequest = () => ({type: DELETE_ORDER_REQUEST});
export const deleteOrderSuccess = (value) => ({type: DELETE_ORDER_SUCCESS, payload: value});
export const deleteOrderFailure = () => ({type: DELETE_DISH_FAILURE});


export const OPEN_MODAL = 'OPEN_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';

export const openModal = () => ({type: OPEN_MODAL});
export const closeModal = () => ({type: CLOSE_MODAL});

export const changeDishRequest = () => ({type: CHANGE_DISH_REQUEST});
export const changeDishSuccess = (value) => ({type: CHANGE_DISH_SUCCESS, payload: value});
export const changeDishFailure = () => ({type: CHANGE_DISH_FAILURE});

export const putDishRequest = () => ({type: PUT_DISH_REQUEST});
export const putDishSuccess = (value) => ({type: PUT_DISH_SUCCESS, payload: value});
export const putDishFailure = () => ({type: PUT_DISH_FAILURE});

export const deleteDishRequest = () => ({type: DELETE_DISH_REQUEST});
export const deleteDishSuccess = (value) => ({type: DELETE_DISH_SUCCESS, payload: value});
export const deleteDishFailure = () => ({type: DELETE_DISH_FAILURE});

export const getOrdersRequest = () => ({type: GET_ORDERS_REQUEST});
export const getOrdersSuccess = (value) => ({type: GET_ORDERS_SUCCESS, payload: value});
export const getOrdersFailure = () => ({type: GET_ORDERS_FAILURE});


export const fetchMenuRequest = () => ({type: FETCH_MENU_REQUEST});
export const fetchMenuSuccess = (response) => ({type: FETCH_MENU_SUCCESS, payload: response,});
export const fetchMenuFailure = () => ({type: FETCH_MENU_FAILURE});

export const postMenuRequest = () => ({type: POST_MENU_REQUEST});
export const postMenuSuccess = () => ({type: POST_MENU_SUCCESS});
export const postMenuFailure = () => ({type: POST_MENU_FAILURE});

export const deleteOrder = (id) => {
    return async (dispatch, getState) => {
        try {
            dispatch(deleteOrderRequest());
            const order = [...getState().menu.orders];
            const index = order.findIndex(item => item.id === id);
            if (index !== -1) {
                order.splice(index, 1);
            }
            dispatch(deleteOrderSuccess(order));
            await axiosAPI.delete('order/' + id + '.json');
        } catch (error) {
            dispatch(deleteOrderFailure(error));
        }
    }
}

export const getOrders = () => {
    return async (dispatch) => {
        try {
            dispatch(getOrdersRequest());
            const response = await axiosAPI.get('order.json');
            const order = Object.keys(response.data).map(order => ({
                ...response.data[order],
            }));
            dispatch(getOrdersSuccess(order))
        } catch (error) {
            dispatch(getOrdersFailure(error));
        }
    }
}

export const removeDish = (id) => {
    return async (dispatch, getState) => {
        try {
            dispatch(deleteDishRequest());
            const pizza = [...getState().menu.menu];
            const index = pizza.findIndex(item => item.id === id);
            if (index !== -1) {
                pizza.splice(index, 1);
            }
            dispatch(deleteDishSuccess(pizza));
            await axiosAPI.delete('menu/' + id + '.json')
        } catch (error) {
            dispatch(deleteDishFailure(error));
        }
    }
}

export const changeDish = (id) => {
    return async (dispatch) => {
        try {
            dispatch(changeDishRequest());
            const response = await axiosAPI.get('menu/' + id + '.json');
            dispatch(changeDishSuccess(response.data));
        } catch (error) {
            dispatch(error);
        }
    }
};

export const putDish = (id, obj) => {
    return async (dispatch) => {
        try {
            dispatch(putDishRequest());
            await axiosAPI.put('menu/' + id + '.json', obj);
            dispatch(putDishSuccess());
        } catch (error) {
            dispatch(putDishFailure(error));
        }
    }
}

export const fetchMenu = () => {
    return async (dispatch) => {
        try {
            dispatch(fetchMenuRequest());
            const response = await axiosAPI.get('menu.json');
            const dishes = Object.keys(response.data).map(dish => ({
                ...response.data[dish],
                id: dish
            }));
            console.log(dishes);
            dispatch(fetchMenuSuccess(dishes));
        } catch (error) {
            dispatch(fetchMenuFailure());
        }
    }
};

export const postMenu = (value) => {
    return async (dispatch) => {
        try {
            dispatch(postMenuRequest());
            await axiosAPI.post('menu.json', value);
            dispatch(postMenuSuccess());
        } catch (error) {
            dispatch(postMenuFailure());
        }
    }
}

