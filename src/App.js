import {BrowserRouter, Switch, Route} from "react-router-dom";
import './App.css';
import Layout from "./Components/UI/Layout/Layout";
import Mainpage from "./Containers/Mainpage/Mainpage";
import Editpage from "./Containers/Editpage/Editpage";
import Orders from './Containers/Orders/Orders'

function App() {
    return (
        <BrowserRouter>
            <Layout>
                <div className="container">
                    <Switch>
                        <Route path={'/'} exact component={Mainpage}/>
                        <Route path={'/orders'} component={Orders} />
                        <Route path={'/edit/:id'} component={Editpage}/>
                    </Switch>
                </div>
            </Layout>
        </BrowserRouter>
    );
}

export default App;
